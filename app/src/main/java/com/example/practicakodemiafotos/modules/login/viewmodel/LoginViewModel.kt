package com.example.practicakodemiafotos.modules.login.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.practicakodemiafotos.data.models.LoginModel
import com.example.practicakodemiafotos.data.models.responses.ResponseLogin
import com.example.practicakodemiafotos.service.ServiceNetwork
import kotlinx.coroutines.launch

class LoginViewModel :ViewModel(){
    //service
    lateinit var service : ServiceNetwork
    //livedatas
    val loginResponse = MutableLiveData<ResponseLogin>()

    val tokenExpirado = MutableLiveData<Boolean>()

    fun onCreate(context: Context){
        service = ServiceNetwork(context)
    }
    //funciones
    fun login(login:LoginModel){
        viewModelScope.launch {
            val response = service.login(login)
            if (response.isSuccessful){
                loginResponse.postValue(response.body())
                //Token expirado o token sin permisos
            }else if (response.code() ==401){
                tokenExpirado.postValue(true)
            }
        }
    }
}