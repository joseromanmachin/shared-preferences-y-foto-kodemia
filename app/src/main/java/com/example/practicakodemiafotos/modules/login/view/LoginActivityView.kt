package com.example.practicakodemiafotos.modules.login.view

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import com.example.practicakodemiafotos.R
import com.example.practicakodemiafotos.core.SharedPreferencesInstance
import com.example.practicakodemiafotos.data.models.LoginModel
import com.example.practicakodemiafotos.databinding.ActivityLoginViewBinding
import com.example.practicakodemiafotos.modules.login.viewmodel.LoginViewModel
import com.example.practicakodemiafotos.modules.main.views.MainActivityView

class LoginActivityView : AppCompatActivity() {

    lateinit var binding : ActivityLoginViewBinding
    val viewModel:LoginViewModel by viewModels()
    val TAG = "Login Test"
    lateinit var shared : SharedPreferencesInstance


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inicializarComponentes()
        observers()


        val token = shared.getToken()

        shared.getToken()?.let {
            startActivity(Intent(this,MainActivityView::class.java))
            Toast.makeText(this,shared.getToken(),Toast.LENGTH_LONG).show()
            finish()
        }

       /* if (shared.getSesion().status){

        }else {
            Toast.makeText(this,R.string.no_token,Toast.LENGTH_LONG).show()
               val login = LoginModel("root@email.com","root")
                mandarDatos(login)
        }*/
      //  Toast.makeText(this,shared.getToken(),Toast.LENGTH_LONG).show()
    }


    private fun observers() {
        viewModel.loginResponse.observe(this){login->
            shared.saveSession(login)
          //  shared.saveToken(it.access_token)
        }

        viewModel.tokenExpirado.observe(this){tokenExpirado ->

            if (tokenExpirado){
                //regresarlo al login
            }
        }
    }




    private fun mandarDatos(login: LoginModel) {
        viewModel.login(login)
    }


    private fun inicializarComponentes() {
        //shared
        shared = SharedPreferencesInstance.getInstance(this)

        //binding
        binding = ActivityLoginViewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.onCreate(context = this)
    }



}