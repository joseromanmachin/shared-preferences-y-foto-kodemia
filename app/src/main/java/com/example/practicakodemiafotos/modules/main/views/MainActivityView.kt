package com.example.practicakodemiafotos.modules.main.views

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.example.practicakodemiafotos.databinding.ActivityMainBinding
import com.example.practicakodemiafotos.modules.main.viewmodels.MainViewmodel
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class MainActivityView : AppCompatActivity() {

    lateinit var binding : ActivityMainBinding
    var archivoFoto :File? = null
    lateinit var absolutePathImagen:String

    val viewModel:MainViewmodel by viewModels()

    private val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
            result ->
        if (result.resultCode == Activity.RESULT_OK){

            val imagen = BitmapFactory.decodeFile(absolutePathImagen)
            binding.imageView.setImageBitmap(imagen)

            archivoFoto?.also { foto->
                viewModel.mandarFoto(foto)
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configuracionInicial()
        observers()

    }

    private fun observers() {
        viewModel.respuestaLiveData.observe(this) {respuesta->
            mensaje(respuesta.data)
            Log.d("Path",respuesta.data)
        }
        viewModel.error.observe(this){error->
            mensaje(error)
        }
    }

    private fun mensaje(mensaje:String) {
        Toast.makeText(this,mensaje,Toast.LENGTH_LONG).show()
    }

    private fun configuracionInicial() {
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.button.setOnClickListener {
            checarPermisos()
        }


    }

    private fun checarPermisos() {
        val REQUEST_CODE_CAMARA = 100
        val permisoCamara = ContextCompat.checkSelfPermission(this,Manifest.permission.CAMERA)
        if (permisoCamara == PackageManager.PERMISSION_GRANTED){
            lanzarCamara()
        }else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                REQUEST_CODE_CAMARA
            )
        }

    }

    @SuppressLint("QueryPermissionsNeeded")
    private fun lanzarCamara() {
      Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { camara->
          camara.resolveActivity(packageManager)?.also {
              archivoFoto  = try {
                  crearImagen()
              }catch (ex:IOException){
                  null
              }

              archivoFoto?.also { archivo ->
                  val photoPATH : Uri = FileProvider.getUriForFile(
                      this,
                      "com.example.practicakodemiafotos.fileprovider",
                      archivo
                  )
                  startForResult.launch(camara)
                 // startForResult.launch(intent)
              }
          }
      }
    }

    @SuppressLint("SimpleDateFormat")
    private fun crearImagen(): File? {
        val timeStamp:String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val directorioAlmacenamiento: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}",
            ".jpeg",
            directorioAlmacenamiento
        ).apply {
            absolutePathImagen = absolutePath
        }
    }
}