package com.example.practicakodemiafotos.modules.main.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.practicakodemiafotos.data.models.Respuesta
import com.example.practicakodemiafotos.service.ServiceNetwork
import kotlinx.coroutines.launch
import java.io.File

class MainViewmodel:ViewModel() {
    //service

    val service = ServiceNetwork()

    //mutables
    val respuestaLiveData = MutableLiveData<Respuesta>()
    val error = MutableLiveData<String>()

    fun mandarFoto(foto : File){
        viewModelScope.launch {
            val respuesta = service.mandarFoto(foto)
            if (respuesta.isSuccessful){
                respuestaLiveData.postValue(respuesta.body())
            }else {
                error.postValue(respuesta.errorBody().toString())
            }

        }
    }

}