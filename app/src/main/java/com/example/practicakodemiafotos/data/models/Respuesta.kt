package com.example.practicakodemiafotos.data.models

data class Respuesta(
    val status:Boolean,
    val message:String,
    val data:String
)
