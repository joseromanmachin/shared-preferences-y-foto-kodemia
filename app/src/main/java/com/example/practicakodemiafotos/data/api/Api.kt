package com.example.practicakodemiafotos.data.api

import com.example.practicakodemiafotos.data.models.LoginModel
import com.example.practicakodemiafotos.data.models.Respuesta
import com.example.practicakodemiafotos.data.models.responses.ResponseLogin
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface Api {
    @Headers("Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9kYXRhb25lc29sdXRpb24uZGRucy5uZXQ6ODAwMlwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTY0NjE5MDIwNCwiZXhwIjoxNjQ2MTkzODA0LCJuYmYiOjE2NDYxOTAyMDQsImp0aSI6IjNCQktXZTVuazgxRlBTV2siLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.nS8vjBIKVVpkUzCGiWxdQ8Nm2r5c5TeLo022mqnnmeg")
    @Multipart
    @POST("api/users/uploadProfile")
    suspend fun mandarFoto(@Part("profile\"; filename=\"pp.jpg\" ") archivo :RequestBody):Response<Respuesta>

    @POST("api/auth/login")
    suspend fun login(@Body loginModel: LoginModel):Response<ResponseLogin>

}