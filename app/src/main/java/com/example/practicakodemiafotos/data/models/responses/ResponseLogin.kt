package com.example.practicakodemiafotos.data.models.responses

data class ResponseLogin(
    val status :Boolean,
    val messaje:String?,
    val userData: userData?,
    val access_token:String?,
    val token_type:String?,
    val expires_in:Int?
)

data class userData(
    val id:Int?,
    val name:String?,
    val first_surname:String?,
    val last_surname:String?,
    val email:String?,
    val email_verified_at:String?,
    val theme:String?,
    val profile:String?,
    val role_id:Int?,
    val created_at:String?,
    val updated_at:String?,
    val full_name:String?

    )
