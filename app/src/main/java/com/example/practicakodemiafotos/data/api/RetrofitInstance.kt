package com.example.practicakodemiafotos.data.api

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.widget.Toast
import com.example.practicakodemiafotos.core.SharedPreferencesInstance
import com.example.practicakodemiafotos.modules.main.views.MainActivityView
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitInstance {
    fun getRetrofit(context: Context): Retrofit {
        val okHttpClient = OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(AuthInterceptor(context = context))
            .build()
        return Retrofit.Builder()
            .baseUrl("http://dataonesolution.ddns.net:8002/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    class AuthInterceptor(context: Context) : Interceptor {
        private val shared = SharedPreferencesInstance.getInstance(context)

        override fun intercept(chain: Interceptor.Chain): Response {
            val requestBuilder = chain.request().newBuilder()

            // If token has been saved, add it to the request

            shared.getToken()?.let {
                requestBuilder.addHeader("Authorization","Barer $it")
            }
           /* sessionManager.fetchAuthToken()?.let {
                requestBuilder.addHeader("Authorization", "Bearer $it")
            }*/

            return chain.proceed(requestBuilder.build())
        }
    }

}