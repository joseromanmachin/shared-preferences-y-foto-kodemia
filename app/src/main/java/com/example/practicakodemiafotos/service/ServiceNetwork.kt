package com.example.practicakodemiafotos.service

import android.content.Context
import com.example.practicakodemiafotos.data.api.Api
import com.example.practicakodemiafotos.data.api.RetrofitInstance
import com.example.practicakodemiafotos.data.models.LoginModel
import com.example.practicakodemiafotos.data.models.Respuesta
import com.example.practicakodemiafotos.data.models.responses.ResponseLogin
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File

class ServiceNetwork (context:Context){
    val retrofit = RetrofitInstance.getRetrofit(context).create(Api::class.java)

    suspend fun mandarFoto(foto:File):Response<Respuesta>{
        //conversion de gile --> requestBody
        val profile = RequestBody.create(MediaType.parse("imagen/*"),foto)

       //Corrutina
        return withContext(Dispatchers.IO){
            val response = retrofit.mandarFoto(profile)
            response
        }
    }

    suspend fun login (login : LoginModel):Response<ResponseLogin>{
        return withContext(Dispatchers.IO){
            val response = retrofit.login(login)
            response
        }
    }
}