package com.example.practicakodemiafotos.core

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.service.autofill.UserData
import com.example.practicakodemiafotos.data.models.responses.ResponseLogin
import com.example.practicakodemiafotos.data.models.responses.userData

object SharedPreferencesInstance {
    //valores
    val sharedPref = SharedPreferencesInstance

    lateinit var sharedPreferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor

    fun getInstance(context: Context):SharedPreferencesInstance{
        sharedPreferences = context.getSharedPreferences(context.packageName,Activity.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        return sharedPref
    }

    fun clean(){
        editor.clear()
        editor.apply()
    }

    fun saveToken(token:String?){
        editor.putString("token",token)
        editor.apply()
    }

    fun saveSession(sesion: ResponseLogin){
        editor.putString("token",sesion.access_token)
        editor.putString("username",sesion.userData?.full_name)
        editor.putString("email",sesion.userData?.email)
        editor.putBoolean("estatus",sesion.status)
        editor.apply()
    }

    fun getSesion():ResponseLogin{
        return ResponseLogin(
            sharedPreferences.getBoolean("estatus",false),
            null,
            userData(0,
                sharedPreferences.getString("username",null),
                "",
                "",
                sharedPreferences.getString("email",null),
                "",
                "",
                "",
                0,
                "",
                "",
                ""),
            sharedPreferences.getString("token",null),
            null,
            null
        )
    }


    fun getToken():String?{
      return sharedPreferences.getString("token",null)
    }



}